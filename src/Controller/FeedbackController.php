<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Form\FeedbackType;
use App\Entity\Feedback;
use App\Service\TgNotifier;

class FeedbackController extends AbstractController
{
    /**
     * @Route("/feedback", name="feedback")
     */
    public function index(Request $request)
    {
        $form = $this->createForm(FeedbackType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $feedback = $form->getData();
            $feedback->setDtCreate(new \DateTime('now'));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($feedback);
            $entityManager->flush();

            $this->tgNotify($feedback);

            return $this->render('feedback/success.html.twig');
        }

        return $this->render('feedback/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function tgNotify(Feedback $feedback)
    {
        $msg_parts = [];

        if (!empty($feedback->getEmail())) {
            $email = trim($feedback->getEmail());
            if (!empty($email))
                $msg_parts[] = 'Email: '.$email;
        }

        if (!empty($feedback->getTg())) {
            $tg = trim($feedback->getTg());
            if (!empty($tg)) {
                $tg = '@'.preg_replace("/^@/", '', $tg);
                $msg_parts[] = 'tg: '.$tg;
            }
        }

        $msg_parts[] = $feedback->getFeedbackText();

        (new TgNotifier())->notify('feedback', implode("\n", $msg_parts));
    }
}

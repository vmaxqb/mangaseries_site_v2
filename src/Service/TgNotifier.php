<?php

namespace App\Service;

class TgNotifier
{
    private $api_url;

    public function __construct()
    {
        $this->api_url = 'https://api.telegram.org/bot'.$_ENV['TG_BOT_TOKEN'].'/sendMessage';
    }

    /**
     * Notifies a developer of a centain event.
     * Does not guarantee the message will be recieved.
     * @param string $reason The reason of notification
     * @param string $message The message of notification
     */
    public function notify(string $reason, string $message): void
    {
        $data = [
            'chat_id'                   => $_ENV['TG_NOTIFICATION_USER'],
            'text'                      => $_ENV['TG_NOTIFICATION_HEAD'].' :: '.$reason."\n\n".$message,
            'parse_mode'                => 'HTML',
            'disable_web_page_preview'  => true,
        ];

        $opts = [
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_POST            => true,
            CURLOPT_URL             => $this->api_url,
            CURLOPT_POSTFIELDS      => json_encode($data),
            CURLOPT_HTTPHEADER      => [
                'Content-Type: application/json; charset=utf-8',
            ],
        ];

        $curl_con = curl_init();
        curl_setopt_array($curl_con, $opts);
        curl_exec($curl_con);
        curl_close($curl_con);
    }
}

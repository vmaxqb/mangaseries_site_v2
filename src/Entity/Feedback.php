<?php

namespace App\Entity;

use App\Repository\FeedbackRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FeedbackRepository::class)
 */
class Feedback
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $feedbackText;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tg;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $dtCreate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFeedbackText(): ?string
    {
        return $this->feedbackText;
    }

    public function setFeedbackText(string $feedbackText): self
    {
        $this->feedbackText = $feedbackText;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTg(): ?string
    {
        return $this->tg;
    }

    public function setTg(?string $tg): self
    {
        $this->tg = $tg;

        return $this;
    }

    public function getDtCreate(): ?\DateTimeInterface
    {
        return $this->dtCreate;
    }

    public function setDtCreate(\DateTimeInterface $dtCreate): self
    {
        $this->dtCreate = $dtCreate;

        return $this;
    }
}
